<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/mainmenu', function () {
    return view('tempmenu');
});

Route::get('/mainmenuhead', function () {
    return view('headmenu');
});

Route::get('/mainmenufooter', function () {
    return view('footer');
});

Route::get('/listsform','ListsController@form');

Route::get('/listsform','ListsController@form');

Route::get('/companyinformation','CompanyController@companyinformation');

Route::get('/companyinformationbyid/{id}','CompanyController@companyinformationbyid');

Route::post('/companyinsert','CompanyController@companyinsert');

Route::post('/deletecompany','CompanyController@deletecompany');

Route::get('/customercenter','CustomerController@customercenter');

Route::get('/configaccounttax','ConfigaccountController@taxfig');


