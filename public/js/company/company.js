/**
 * Created by pacharapol on 1/3/2018 AD.
 */
$(document).ready(function() {
    // $('#Tel').mask('999-999-9999');
    // $('#Fax').mask('999-999-9999');
    // $('#soc_acc_number').mask('99-9999999-9');
    // $('#business_number').mask('9-9999-99999-9-999');



    // insert //
    $('#Tel').mask('999-999-9999');
    $('#Fax').mask('999-999-9999');
    $('#soc_acc_number').mask('99-9999999-9');
    $('#business_number').mask('9-9999-99999-9-999');




});


function getdatacompany(id) {
    var idcompany = id;

    $.ajax({
        url: 'companyinformationbyid/'+idcompany,
        type: 'GET',
        success: function (data, textStatus, jQxhr) {
            //console.log(data);
            $('#name').val(data[0].name);
            $('#codeName_tax').val(data[0].codeName_tax);
            $('#short_name').val(data[0].short_name);
            $('#name_eng').val(data[0].name_eng);

            $('#address').val(data[0].address);
            $('#Tel').val(data[0].Tel);
            $('#Fax').val(data[0].Fax);

            $('#soc_acc_number').val(data[0].soc_acc_number);
            $('#business_number').val(data[0].business_number);
            $('#account_name').val(data[0].account_name);
            $('#account_number').val(data[0].account_number);

            if (data[0].company_type == '1') {

                $("#TypeCompany1").attr('checked', 'checked');
            } else {

                $("#TypeCompany2").attr('checked', 'checked');
            }

        },
        error: function (jqXhr, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });


}

function getdatesubmit() {

    var valid = $('#companyFormxxx').validator('validate').has('.has-error').length;
    if (valid == 0) {
        Savecompany();
    }
    return false;
}

function Savecompany() {
    var companyInpus = null;
    companyInpus = $('#companyFormxxx').serializeArray();

    if (companyInpus.company_type = "1") {
        companyInpus.branch_comment = "สำนักงานใหญ่";
    } else if (companyInpus.company_type = "2") {
        companyInpus.branch_comment = "สาขา";
    }
    var company = JSON.stringify(companyInpus);


        $.post('companyinsert', {companyInsert: company}, function (res) {
            //console.log(res);
            if (res == 1) {
                bootbox.alert({
                    title: "แจ้งเตือน",
                    message: "บันทึกข้อมูลเรียบร้อยแล้ว!!",
                    callback: function () {
                        $('#ConfirmDialog').modal('hide');
                        $('#myModal').modal('hide');
                        location.reload();

                    }
                });
            }
        });
}


function canceldatacompany(id) {

    var companyinsert = id;

    bootbox.confirm({
        size: "small",
        message: "<h4 class=\"btalert\">ยืนยันการลบรายการ? </h4>",
        callback: function(result) {
            if (result == 1) {
                $.post('deletecompany', {id: companyinsert}, function (res) {
                    //console.log(res);
                    if (res == 1) {
                        bootbox.alert({
                            title: "แจ้งเตือน",
                            message: "บันทึกข้อมูลเรียบร้อยแล้ว!!",
                            callback: function () {
                                location.reload();
                            }
                        });
                    }
                });
            }
        }
    });






}