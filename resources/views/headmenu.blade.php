<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FSCT Account</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <!-- Morris chart -->
    {{--<link rel="stylesheet" href="bower_components/morris.js">--}}
    <!-- jvectormap -->
    {{--<link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">--}}
    {{--<!-- Date Picker -->--}}
    {{--<link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">--}}
    {{--<!-- Daterange picker -->--}}
    {{--<link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">--}}
    {{--<!-- bootstrap wysihtml5 - text editor -->--}}
    {{--<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">--}}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>-->
    <!--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="index2.html" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>AC</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>ACCOUNT</b></span>
        </a>



        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <li  class="dropdown ">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Lists <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Chart Accounts</a></li>
                            <li><a href="#">Item List</a></li>
                            <li><a href="#">Fixed Asset Item List</a></li>
                            <li><a href="#">Price Level List</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Other Names List</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Memorized Transaction List</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Add/Edit Multiple List Entries</a></li>

                        </ul>

                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Vendor Profile Lists<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Sales Rep List</a></li>
                            <li><a href="#">Customer Type List</a></li>
                            <li><a href="#">Vendor Type List</a></li>
                            <li><a href="#">Job Type List</a></li>
                            <li><a href="#">Terms List</a></li>
                            <li><a href="#">Customer Message List</a></li>
                            <li><a href="#">Payment Method List</a></li>
                            <li><a href="#">Ship Via List</a></li>
                            <li><a href="#">Vehicle List</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Company<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo url("/");?>">Home Page</a></li>
                            <li><a href="#">Company Snapshot</a></li>
                            <li><a href="#">Calendar</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Documents Center</a></li>
                            <li><a href="#">Clean Up Attachment Links</a></li>
                            <li><a href="#">Repair Attached Document Links</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Lead Center</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo url("/companyinformation");?>">Company Information</a></li>
                            <li><a href="#">Advanced Service Administration</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Set Up Budgets</a></li>
                            <li><a href="#">Cash Flow Projector</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Chart of Accounts</a></li>
                            <li><a href="#">Make General Journal Entries</a></li>
                            <li><a href="#">Lean About Multicurrency</a></li>
                            <li><a href="#">Multicurrency Resource Center</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Enter Vehicle Mileage</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Customers<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo url("/customercenter");?>">Customer Center</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Create Estimates</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Create Invoices</a></li>
                            <li><a href="#">Create Batch Invoices</a></li>
                            <li><a href="#">Enter Sales Receipts</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Enter Statement Charges</a></li>
                            <li><a href="#">Create Statements</a></li>
                            <li><a href="#">Assess Finance Charges</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Receive Payments</a></li>
                            <li><a href="#">Create Credit Memos/Refunds</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Lead Center</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Add Credit Card Processing</a></li>
                            <li><a href="#">Link Payment Service to Company File</a></li>
                        </ul>
                    </li>

                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Vendors<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Vendors Center</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Enter Bills</a></li>
                            <li><a href="#">Pay Bills</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Employees<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Employee Center</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Banking<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Write Checks</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Order Checks</a></li>
                            <li><a href="#">Set Checks Reorder Reminder</a></li>
                            <li><a href="#">Write Checks</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">ตั้งค่า<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo url("/configaccounttax");?>">ตั้งค่าเลขบัญชี</a></li>
                            <li><a href="#">ตั้งค่าประเภทการซื้อ</a></li>
                            <li><a href="#">ตั้งค่าประเภทการจ่าย</a></li>
                            <li><a href="#">ตั้งค่าภาษี</a></li>
                            <li><a href="#">ตั้งค่า terms</a></li>
                            <li><a href="#">ตั้งค่ากลุ่มจัดซื้อ</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs">Alexander Pierce</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                                <p>
                                    Alexander Pierce - Web Developer
                                    <small>Member since Nov. 2012</small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="row">
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Followers</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Sales</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Friends</a>
                                    </div>
                                </div>
                                <!-- /.row -->
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="#" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>Alexander Pierce</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-home"></i> <span>My Shortcuts</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-home"></i> Home </a></li>
                        <li><a href="#"><i class="fa  fa-calendar"></i> Calendar</a></li>
                        <li><a href="#"><i class="fa  fa-calendar"></i> Snapshots</a></li>
                        <li><a href="#"><i class="fa  fa-calendar"></i> Customers</a></li>
                        <li><a href="#"><i class="fa  fa-calendar"></i> Vendors</a></li>
                        <li><a href="#"><i class="fa  fa-calendar"></i> Employees</a></li>
                        <li><a href="#"><i class="fa  fa-calendar"></i> Online Banking</a></li>
                        <li><a href="#"><i class="fa  fa-calendar"></i> Docs</a></li>
                        <li><a href="#"><i class="fa  fa-calendar"></i> Report</a></li>
                        <li><a href="#"><i class="fa  fa-calendar"></i> Order Checks</a></li>
                    </ul>
                </li>
                </span>

                </span>
                </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-pie-chart"></i>
                        <span>Do Today <?php echo date('d/m/Y');?></span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i> TRANSACTIONS DUE(0)</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i> TO DO(0)</a></li>
                        <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Reminders</a></li>
                        <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Alerts Manager</a></li>
                        <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Add New To Do</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-pie-chart"></i>
                        <span>View Balances</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i> Accounts Payable (0.00)</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i> Accounts Receivable (0.00)</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-pie-chart"></i>
                        <span>Report</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i> Report Center</a></li>
                        <li class="treeview">
                            <a href="#"><i class="fa fa-circle-o"></i> Memorized Reports
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                                    </a>
                            <ul class="treeview-menu" style="display: none;">
                                <li><a href="#"><i class="fa fa-circle-o"></i>Memorized Report List</a></li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Accountant
                                <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                                </span>
                                    </a>
                                    <ul class="treeview-menu" style="display: block;">
                                        <li><a href="#"><i class="fa fa-circle-o"></i>Balance Sheet</a></li>
                                        <li><a href="#"><i class="fa fa-circle-o"></i>General Ledger</a></li>
                                        <li><a href="#"><i class="fa fa-circle-o"></i>Journal Entries Entered/<br>
                                            Modified Today
                                            </a></li>
                                        <li><a href="#"><i class="fa fa-circle-o"></i>General Ledger</a></li>
                                    </ul>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Banking
                                        <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                                </span>
                                    </a>
                                    <ul class="treeview-menu" style="display: block;">
                                        <li><a href="#"><i class="fa fa-circle-o"></i>Check Detail</a></li>
                                        <li><a href="#"><i class="fa fa-circle-o"></i>Deposit Detail</a></li>
                                    </ul>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Company
                                        <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                                </span>
                                    </a>
                                    <ul class="treeview-menu" style="display: block;">
                                        <li><a href="#"><i class="fa fa-circle-o"></i>Balance Sheet</a></li>
                                        <li><a href="#"><i class="fa fa-circle-o"></i>Profit & Loss</a></li>
                                        <li><a href="#"><i class="fa fa-circle-o"></i>Statement of Cash Flows</a></li>
                                    </ul>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Customers
                                        <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                                </span>
                                    </a>
                                    <ul class="treeview-menu" style="display: block;">
                                        <li><a href="#"><i class="fa fa-circle-o"></i>A/R Aging Summary</a></li>
                                        <li><a href="#"><i class="fa fa-circle-o"></i>Customer Balance Detail</a></li>
                                        <li><a href="#"><i class="fa fa-circle-o"></i>Customer Balance <br> Summary</a></li>
                                        <li><a href="#"><i class="fa fa-circle-o"></i>Open Invoice</a></li>
                                    </ul>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Vendors
                                        <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                                </span>
                                    </a>
                                    <ul class="treeview-menu" style="display: block;">
                                        <li><a href="#"><i class="fa fa-circle-o"></i>A/P Aging Summary</a></li>
                                        <li><a href="#"><i class="fa fa-circle-o"></i>Unpaid Bills Detail</a></li>
                                        <li><a href="#"><i class="fa fa-circle-o"></i>Vendor Balance Detail</a></li>
                                        <li><a href="#"><i class="fa fa-circle-o"></i>Vendor Balance <br> Summary</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="#"><i class="fa fa-circle-o"></i> Company Snapshot</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i> Process Multiple Report</a></li>
                        <li class="treeview">
                            <a href="#"><i class="fa fa-circle-o"></i> Company & Financial
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu" style="display: none;">

                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Profit & Loss Standard
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Profit & Loss Detail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Profit & Loss <br>
                                        YTD Comparison
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Profit & Loss <br>
                                        Prev Year Comparison
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Profit & Loss by Job
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Profit & Loss by Class
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Profit & Loss by Unclassified
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Income by Customer <br>
                                        Summary
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Income by Customer <br>
                                        Detail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Expenses by Vendor <br>
                                        Summary
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Expenses by Vendor <br>
                                        Detail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Income & Expense <br>
                                        Graph
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Balance Sheet Standard
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Balance Sheet Detail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Balance Sheet Summary
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Balance Sheet <br>
                                        Prev Year Comparison
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Net Worth Graph
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Statement of Cash Flows
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Cash Flow Forecast
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#"><i class="fa fa-circle-o"></i> Customers & Receivables
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu" style="display: none;">

                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>A/R Aging Summary
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>A/R Aging Detail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Customer Balance <br> Summary
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Customer Balance <br> Detail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Open Invoices
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Collections Report
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Average Days to  Pay <br> Summary
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Average Days to  Pay
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Accounts Receivable Graph
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Unbilled Costs by Job
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Transaction List <br> by Custommer
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Customer Phone List
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Customer Contact List
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Item Price List
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#"><i class="fa fa-circle-o"></i> Sales
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu" style="display: none;">

                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Sales by Customer Summary
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Sales by Customer Detail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Sales by Item Summary
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Sales by Item Detail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Sales by Rep Summary
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Sales by Rep Detail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Sales by Ship To Address
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Sales Graph
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Pending Sales
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#"><i class="fa fa-circle-o"></i> Jobs, Time & Mileage
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu" style="display: none;">

                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Jobs Profitability Summary
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Jobs Profitability Detail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Jobs Profitability vs. <br>
                                        Actuals Summary
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Jobs Profitability vs. <br>
                                        Actuals Detail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Item Profitability
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Item Estimates vs. Actuals
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Profit & Loss by Job
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Estimates by Job
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Unbilled Costs by Job
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Open Purchase Orders <br> by Job
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>

                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Time by Job Summary
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Time by Job Detail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Time by Name
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Time by Item
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Mileage by Vehicle Summary
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Mileage by Vehicle Detail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Mileage by Job Summary
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Mileage by Job Detail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#"><i class="fa fa-circle-o"></i> Vendors & Payables
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu" style="display: none;">

                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>A/P Aging Summary
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>A/P Aging Detail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Vendor Balance Summary
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Vendor Balance Detail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Unpaid Bills Detail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Accounts Payable Graph
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Transaction List by Vendor
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>1099 Summary
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>1099 Detail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Vendor Phone List
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>

                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Vendor Contact List
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#"><i class="fa fa-circle-o"></i> Banking
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu" style="display: none;">

                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Deposit Detail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Check Detail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Missing Checks
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Reconciliation Discrepancy
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Previous Reconciliation
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#"><i class="fa fa-circle-o"></i> Accountant & Taxes
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu" style="display: none;">

                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Trial Balance
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>General Ledger
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Transaction Detail <br> by Account
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Journal
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Audit Trail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Customer Credit Card <br> Audit Trail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Voided/Deleted <br> Transactions  Summary
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Voided/Deleted <br> Transactions  Detail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Transaction List by Date
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Account Listing
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Fixed Asset Listing
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Income Tax Preparation
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Income Tax Summary
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Income Tax Detail
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#"><i class="fa fa-circle-o"></i> Budgets
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu" style="display: none;">

                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Budgets Overview
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Budgets vs. Actual
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Profit & Loss  <br> Budget Performance
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Budgets vs. Actual Graph
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>

                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#"><i class="fa fa-circle-o"></i> List
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu" style="display: none;">

                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Account Listing
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Item Price List
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Item Price List <br>
                                        for Price Level
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Item Listing
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Fixed Asset Listing
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Customer Phone List
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Customer Contact List
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Vendor Phone List
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Vendor Contact List
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Employee Contact List
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Other Names Phone List
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Other Names Contact List
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Terms Listing
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>To Do Note
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Memorized Transaction Listing
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>

                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#"><i class="fa fa-circle-o"></i> Contributed Report
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu" style="display: none;">

                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Company & Financial
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Customer & Receivables
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Sales
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Jobs, Time & Mileage
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Vendors & Payables
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Purchases
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Inventory
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Employee & Payroll
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Banking
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Accountant & Taxes
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>Budgets & Forecasts
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i>List
                                        <span class="pull-right-container"></span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>


            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->

    <!-- /.content-wrapper -->


    <!-- Control Sidebar -->

    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->


<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{--<script src="dist/js/pages/dashboard.js"></script>--}}
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
