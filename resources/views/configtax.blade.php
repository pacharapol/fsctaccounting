@include('headmenu')
<link>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}
<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>
<script type="text/javascript" src = 'js/company/company.js'></script>
<script type="text/javascript" src = 'src/jquery.mask.js'></script>
<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">ตั้งค่า</a>
                    </li>
                    <li class="active">ตั้งค่าภาษี</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        ตั้งค่าภาษี
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <?php

                                   // print_r($users);
                                    ?>
                                        <table class="table">
                                            <thead class="thead-inverse">
                                            <tr>
                                                <th>#</th>
                                                <th> ภาษี (%)</th>
                                                <th>รายละเอียด</th>
                                                <th>สถานะ</th>
                                                <th colspan="2">การจัดการ</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')


<!-- Modal -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">ข้อมูลบริษัท</h5>
            </div>

            <div class="modal-body">
                <!-- The form is placed inside the body of modal -->
                <form id="companyFormxxx" onsubmit="return getdatesubmit();" data-toggle="validator" method="post" class="form-horizontal">
                    <input value="{{ null }}" type="hidden" id="id" name="id" disable/>

                    <div class="form-group">
                        <label class="col-xs-3 control-label">ชื่อบริษัท<i><span style="color: red">*</span></i></label>
                        <div class="col-xs-5">
                            <input type="text" id="name" class="form-control" name="name" required/>
                        </div>
                        <div class="col-xs-4">
                            <label class="control-label"> ตัวอย่าง บริษัท ฟ้าใส<br>แมนูแฟคเจอริ่ง จำกัด</label>
                        </div>
                    </div>
                    {{--<input value="0" type="hidden" id="check" name="check"/>--}}
                    <div class="form-group">
                        <label class="col-xs-3 control-label">ชื่อบริษัท [Eng]<i><span style="color: red">*</span></i></label>
                        <div class="col-xs-5">
                            <input type="text" id="name_eng" class="form-control" name="name_eng" required/>
                        </div>
                        <div class="col-xs-4">
                            <label class="control-label" style="text-align:left">ตัวอย่าง Fasai Manufacturing Co.,Ltd </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">ชื่อย่อบริษัท<i><span style="color: red">*</span></i></label>
                        <div class="col-xs-5">
                            <input type="text" id="short_name" class="form-control" name="short_name" required/>
                        </div>
                        <div class="col-xs-4">
                            <label class="control-label" style="text-align:left">ตัวอย่าง FSM</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">รหัสบริษัท<i><span style="color: red">*</span></i></label>
                        <div class="col-xs-5">
                            <input type="text" id="code_name" class="form-control" name="code_name" required/>
                        </div>
                        <div class="col-xs-4">
                            <label class="control-label" style="text-align:left">ตัวอย่าง FSM</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">ที่อยู่บริษัท</label>
                        <div class="col-xs-5">
                            <input type="text"  id="address" class="form-control" name="address" />
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">เบอร์โทร</label>
                        <div class="col-xs-5">
                            <input type="text"  id="Tel" class="form-control" name="Tel" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Fax</label>
                        <div class="col-xs-5">
                            <input type="text" id="Fax"  name="Fax" class="form-control" data-inputmask="'mask': ['999-999-999']" data-mask>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">เลขประจำตัวผู้เสียภาษี</label>
                        <div class="col-xs-5">
                            <input type="text" class="form-control" id="business_number"  name="business_number" data-inputmask="'mask': ['9-9999-99999-9-99']" data-mask>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">เลขบัญชีประกันสังคม</label>
                        <div class="col-xs-5">
                            <input type="text" class="form-control" id="soc_acc_number"  name="soc_acc_number" data-inputmask="'mask': ['99-9999999-9']" data-mask></data-mask>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">ประเภทบริษัท</label>
                        <div class="col-xs-5">
                            <div class="form-group">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="company_type" id="TypeCompany1" value="1" checked="">
                                        สำนักงานใหญ่
                                    </label>
                                    <label>
                                        <input type="radio" name="company_type" id="TypeCompany2" value="2">
                                        สาขา
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">ชื่อบัญชีธนาคาร</label>
                        <div class="col-xs-5">
                            <input type="text" class="form-control" id="account_name"  name="account_name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">เลขบัญชีธนาคาร</label>
                        <div class="col-xs-5">
                            <input type="text" class="form-control" id="account_number"  name="account_number" data-inputmask="'mask': ['999-9-99999-9']" data-mask>
                        </div>
                    </div>
                    <!--                    <div class="form-group">-->
                    <!--                        <label class="col-xs-3 control-label">dealercode</label>-->
                    <!--                        <div class="col-xs-5">-->
                    <!--                            <input type="text" id="Code" class="form-control" name="dealercode" />-->
                    <!--                        </div>-->
                    <!--                    </div>-->

                    <div class="form-group">
                        <div class="col-xs-5 col-xs-offset-3">
                            <button type="submit" id="Btn_save" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
