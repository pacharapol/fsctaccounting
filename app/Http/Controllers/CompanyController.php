<?php
/**
 * Created by PhpStorm.
 * User: pacharapol
 * Date: 12/23/2017 AD
 * Time: 9:38 AM
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use  App\Api\Connectdb;
use App\working_company;
use DB;
use Illuminate\Support\Facades\Input;



class CompanyController extends Controller
{
    public function __construct()
    {

    }

    public function companyinformation(){
        return view('companyinformation');
    }


    public function companyinformationbyid($id){

        $db = Connectdb::Databaseall();
        //print_r($db);

         $sql = "SELECT $db[hr_base].working_company.* 
                     FROM $db[hr_base].working_company
                     WHERE $db[hr_base].working_company.id = '$id'";

        $dataquery = DB::connection('mysql2')->select($sql);

        return $dataquery;
    }


    public function companyinsert(){
        $companyinsert = Input::all();
        $companyinsert = json_decode($companyinsert['companyInsert']);

        $arrInsert = [];

        foreach ($companyinsert as $v){
            $arrInsert [$v->name] = $v->value;
        }
        $model = DB::connection('mysql2')->table('working_company')->insert($arrInsert);
        //print_r($arrInsert);

        return 1;
    }


    public function deletecompany(){
        $id = Input::all();
        //print_r($id);
        DB::connection('mysql2')->table('working_company')
            ->where('id', $id['id'])
            ->update(['status' => 99]);
        return 1;
    }
}